(function () {
    const generatePassword = createPasswordGenerator();
    const btnAction = document.getElementById('actionButton');
    const btnCopy = document.getElementById('copyButton');
    const input = document.getElementById('outputInput');

    btnAction.addEventListener('click', handleActionButtonClick);
    btnCopy.addEventListener('click', handleCopyClick);

    const setGeneratedPassword = (function (input) {
        return function (password) {
            input.value = password;
        }
    })(input);
    const copyText = (function (input) {
        return function () {
            /* Select the text field */
            input.select();
            input.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");
        }
    })(input);

    function handleActionButtonClick() {
        setGeneratedPassword(generatePassword(12));
    }

    function handleCopyClick() {
        copyText();
    }

    handleActionButtonClick();
})()