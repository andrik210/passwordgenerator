const createPasswordGenerator = function(allowedLowerAlphabet, allowedUpperAlphabet, allowedSymbols, allowedNumbers) {
    var ALLOWED_LOWER_ALPHABET = allowedLowerAlphabet || "abcdefghijklmnopqrstuvwxyz";
    var ALLOWED_UPPER_ALPHABET = allowedUpperAlphabet || "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var ALLOWED_SYMBOLS = allowedSymbols || "!@#$%&";
    var ALLOWED_NUMBERS = allowedNumbers || "0123456789";

    function getRandomNumber(min, max) {
        return Math.floor(Math.random() * (max + 1 - min) + min);
    }
    
    //selectArray is  a utility function that is used to 
    //randomly generate a integer in the range 1 to 4 (both inclusive)
    function selectArray()
    {
        return getRandomNumber(1, 4);
    }
    
    //getKey() is another utility function that is used to randomly generate 
    //an integer in the range 0 to 25 (both inclusive)
    function getKey()
    {
        return getRandomNumber(0, 25);
    }
    
    function generatePassword(length)
    {
        //Intializing result string password as NULL.
        var password = "";
    
        //intializing local variables
        var key, countAlphabet = 0, countALPHABET = 0, countNumber = 0, countSymbol = 0;
    
        //Count will store the length of the password being created,initially this will be zero(0)
        var count = 0;
        while (count < length) {
            // selectArray() function will return a number 1 to 4
            // and will use to select one of the above defined string 
            //(i.e alphabet or ALPHABET or s_symbol or number )
            // 1 is for string alphabet
            // 2 is for string ALPHABET
            // 3 is for string number
            // and 4 is for string s_symbol
    
            var k = selectArray();
    
            //for the first character of password it is mentioned that ,it should be a letter
            // so the string that should be selected is either alphabet or ALPHABET (i.e 1 or 2)
            // following if condition will take care of it.
            if (count == 0) {
                k = k % 3;
                if (k == 0)
                    k++;
            }

            switch (k) {
                case 1:
                    // following if condition will check if minimum requirement of alphabet 
                    // character has been fulfilled or not
                    // incase it has been fulfilled and minimum requirements of other 
                    // characters is still left then it will break ;
                    if ((countAlphabet == 2) && (countNumber == 0 || countALPHABET == 0 || countALPHABET == 1 || countSymbol == 0))
                        break;
    
                    key = getKey();
                    password += ALLOWED_LOWER_ALPHABET[key];
                    countAlphabet++;
                    count++;
                    break;
    
                case 2:
                    // following if condition will check if minimum requirement of 
                    // ALPHABET character has been fulfilled or not
                    // incase it has been fulfilled and minimum requirements of 
                    // other characters is still left then it will break ;
                    if ((countAlphabet == 2) && (countNumber == 0 || countAlphabet == 0 || countAlphabet == 1 || countSymbol == 0))
                        break;
                    key = getKey();
                    password += ALLOWED_UPPER_ALPHABET[key];
                    countALPHABET++;
                    count++;
                    break;
    
                case 3:
                    // following if condition will check if minimum requirement 
                    // of Numbers  has been fulfilled or not
                    // incase it has been fulfilled and minimum requirements of 
                    // other characters is still left then it will break ;
                    if ((countNumber == 1) && (countAlphabet == 0 || countAlphabet == 1 || countALPHABET == 1 || countALPHABET == 0 || countSymbol == 0))
                        break;
    
                    key = getKey();
                    key = key % 10;
                    password += ALLOWED_NUMBERS[key];
                    countNumber++;
                    count++;
                    break;
    
                case 4:
                    // following if condition will check if minimum requirement of 
                    // Special symbol character has been fulfilled or not
                    // incase it has been fulfilled and minimum requirements of 
                    // other characters is still left then it will break ;
                    if ((countSymbol == 1) && (countAlphabet == 0 || countAlphabet == 1 || countALPHABET == 0 || countALPHABET == 1 || countNumber == 0))
                        break;
    
                    key = getKey();
                    key = key % 6;
                    password += ALLOWED_SYMBOLS[key];
                    countSymbol++;
                    count++;
                    break;
            }
        }

        return password;
    }

    return generatePassword;
};
