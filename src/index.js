const http = require("http");
const url = require("url");
const path = require("path");
const fs = require("fs");
const port = process.argv[2] || 8080;
const hostname = '127.0.0.1';
const publicFolder = 'public';

const server = http.createServer((req, res) => {
    var uri = url.parse(req.url).pathname;
    var publicFolderPath = path.join(process.cwd(), publicFolder);
    var filename = path.join(publicFolderPath, uri);

    var contentTypesByExtension = {
        '.html': "text/html",
        '.css': "text/css",
        '.js': "text/javascript"
    };

    fs.exists(filename, function (exists) {
        if (!exists) {
            filename = publicFolderPath;
        }

        if (fs.statSync(filename).isDirectory()) filename += '/index.html';

        fs.readFile(filename, "binary", function (err, file) {
            if (err) {
                res.writeHead(500, { "Content-Type": "text/plain" });
                res.write(err + "\n");
                res.end();
                return;
            }

            var headers = {};
            var contentType = contentTypesByExtension[path.extname(filename)];
            if (contentType) headers["Content-Type"] = contentType;
            res.writeHead(200, headers);
            res.write(file, "binary");
            res.end();
        });
    });

});

server.listen(parseInt(port, 10), hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});